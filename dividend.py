# -*- coding: utf-8 -*-
from __future__ import division
import peewee
import itertools

from db import *


class Dividend:
    race_id = None
    cached_dividend_by_race = None

    def __init__(self, race_id):
        self.race_id = race_id

    def list_to_int(self, str_list):
        r = [int(i) for i in str_list]
        return r

    def get_race_number(self):
        r = None
        query = Races.select().where(Races.id == self.race_id).get()

        r = query.race_number

        return r

    def get_previous_race_id(self):
        r = None
        query = Races.select().where(Races.id == self.race_id).get()

        if query.race_number > 1:
            race_number = query.race_number - 1
            meeting_id = query.meeting_id
            query = Races.select().where(
                (Races.meeting_id == meeting_id) &
                (Races.race_number == race_number)
            ).get()
            r = query.id

        return r

    def get_win_odds_by_number(self, number):
        r = None
        query = Results.select().where(
            (Results.race_id == self.race_id) &
            (Results.number == number)
        ).get()

        if query.win_odds > 0:
            r = query.win_odds

        return r

    def get_dividend_by_race(self):
        if self.cached_dividend_by_race is None:
            self.cached_dividend_by_race = Dividends.select().where(
                Dividends.race_id == self.race_id).order_by(Dividends.id)
        return self.cached_dividend_by_race

    def get_results(self):
        r = Results.select().where(Results.race_id == self.race_id).order_by(
            Results.running_position).limit(4)
        return r

    def get_dividend_by_pool(self, pool):
        r = {}

        query = self.get_dividend_by_race()

        for i in query:
            match = False
            if pool == i.pool:
                match = True
            if pool == '孖T' or pool == 'double_trio':
                if pool in i.pool:
                    # 第一口孖T/第二口孖T/第三口孖T
                    match = True
            if match:
                r[i.winning_combination] = i.dividend

        return r

    """
        argument bet are horse list with different combination
        horses = [1, 2, 3]
        quinella = list(itertools.permutations(horses, 2))
        match_dividend([(1, 2), (1, 3), (2, 3)], '連贏')
        match_dividend([(1, 2), (1, 3), (2, 3)], '位置Q')
    """

    def match_dividend(self, bet, pool):
        r = {}
        dividend = self.get_dividend_by_pool(pool)

        for b in bet:
            if '-' in dividend:  # 退款
                print('Race %s has no dividend' % self.race_id)
                continue
            horses = self.list_to_int(b)
            # 獨贏, winner
            # 位置, place
            # 連贏, first two, any order
            # 位置Q, two of first three, any order
            # 單T, first three, any order
            # 四連環, first four, any order
            if pool == "獨贏" or pool == "win" or \
                pool == "位置" or pool == "place" or \
                    pool == "連贏" or pool == "quinella" or \
                        pool == "位置Q" or pool == "quinella_place" or \
                            pool == "單T" or pool == "trio" or \
                                pool == "四連環" or pool == "first_4":
                for i in dividend:
                    w = i.split(',')
                    w = self.list_to_int(w)
                    if set(horses) == set(w):
                        r[i] = dividend[i]

            # 三重彩, first three, ordered
            # 四重彩, first four, ordered
            if pool == "三重彩" or pool == "tierce" or \
                    pool == "四重彩" or pool == "quartet":
                # the horse order in HKJC dividend report are not in correct ordered!
                c = 0
                w = []
                results = self.get_results()
                for i in results:
                    w.append(i.number)
                    c = c + 1
                    if c == 4:
                        break
                w = self.list_to_int(w)
                # now we have the horses in correct order

                for i in dividend:
                    if pool == "三重彩" or pool == "tierce":
                        if (horses[0] == w[0]) and (horses[1] == w[1]) and (horses[2] == w[2]):
                            r[i] = dividend[i]
                    if pool == "四重彩" or pool == "quartet":
                        if (horses[0] == w[0]) and (horses[1] == w[1]) and (horses[2] == w[2]) and (horses[3] == w[3]):
                            r[i] = dividend[i]

        if len(r) == 0:
            r = None

        return r

    '''
        孖T, first three unordered in two selected races
        bet = {race_number1: [bet_list1], race_number2: [bet_list2]}
        e.g. bet = {6: {'trio': [(5, 4, 9)]}, 5: {'trio': [(3, 6, 10)]}}

        match_dividend_for_double_trio(bet, 6)
    '''

    def match_dividend_for_double_trio(self, bet, current_race_number):
        r = {}
        valid = False
        if current_race_number > 1:
            current_race_number_1 = current_race_number - 1
            if current_race_number_1 in bet:
                # previous race exist
                if ('trio' in bet[current_race_number]) and ('trio' in bet[current_race_number_1]):
                    # trio bet exist
                    valid = True

        if valid:
            dt_dividend = self.get_dividend_by_pool('孖T')
            if len(dt_dividend) > 0:
                # get current race bet
                r2_race_bet = bet[current_race_number]['trio']
                r2_dividend = self.match_dividend(r2_race_bet, '單T')
                if r2_dividend is not None:
                    # we have a trio payoff for this race, let's see if previous race won trio as well
                    r1_race_id = self.get_previous_race_id()
                    if r1_race_id is not None:
                        r1_race_number = current_race_number - 1
                        r1_race_bet = bet[r1_race_number]['trio']
                        r1_dividend = Dividend(r1_race_id)
                        r1_dividend = r1_dividend.match_dividend(
                            r1_race_bet, '單T')
                        if r1_dividend is not None:
                            r = dt_dividend

        if len(r) == 0:
            r = None

        return r

    '''
        三T, first three unordered in three selected races
        bet = {race_number1: [bet_list1], race_number2: [bet_list2], race_number3: [bet_list3]}
        e.g. bet = {6: {'trio': [(5, 4, 9)]}, 5: {'trio': [(3, 6, 10)]}, 4: {'trio': [(3, 4, 7)]}}

        match_dividend_for_triple_trio(bet, 6)
    '''

    def match_dividend_for_triple_trio(self, bet, current_race_number):
        r = {}
        valid = False
        if current_race_number > 2:
            current_race_number_1 = current_race_number - 1
            current_race_number_2 = current_race_number - 2
            if current_race_number_1 in bet and current_race_number_2 in bet:
                # previous race exist
                if ('trio' in bet[current_race_number]) and ('trio' in bet[current_race_number_1]) and ('trio' in bet[current_race_number_2]):
                    # trio bet exist
                    valid = True

        if valid:
            dt_dividend = self.get_dividend_by_pool('三T')
            if len(dt_dividend) > 0:
                # get current race bet
                r3_race_bet = bet[current_race_number]['trio']
                r3_dividend = self.match_dividend(r3_race_bet, '單T')
                if r3_dividend is not None:
                    # we have a trio payoff for this race, let's see if previous race won trio as well
                    r2_race_id = self.get_previous_race_id()
                    if r2_race_id is not None:
                        r2_race_number = current_race_number - 1
                        r2_race_bet = bet[r2_race_number]['trio']
                        r2_dividend = Dividend(r2_race_id)
                        r2_dividend = r2_dividend.match_dividend(
                            r2_race_bet, '單T')
                        if r2_dividend is not None:
                            # we have a trio payoff for this race, let's see if previous race won trio as well
                            r2_dividend = Dividend(r2_race_id)
                            r1_race_id = r2_dividend.get_previous_race_id()
                            if r1_race_id is not None:
                                r1_race_number = r2_race_number - 1
                                r1_race_bet = bet[r1_race_number]['trio']
                                r1_dividend = Dividend(r1_race_id)
                                r1_dividend = r1_dividend.match_dividend(
                                    r1_race_bet, '單T')
                                if r1_dividend is not None:
                                    r = dt_dividend

        if len(r) == 0:
            r = None

        return r
