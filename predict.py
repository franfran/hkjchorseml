# -*- coding: utf-8 -*-
import pandas as pd
from keras.models import load_model

import sys
import os
import traceback

from datetime import datetime, timedelta
from db import database, Entries


def load_data(features_name, meeting_date, drop_columns):
    db_conn = database.connection()
    df = pd.read_sql(
        ('SELECT * FROM entries'+features_name+' where meeting_date = "%s" order by race_number, number' % meeting_date), con=db_conn)
    df = df.drop(columns=drop_columns)

    return df


def predict(features_name, meeting_date, drop_columns):
    model = load_model('data/'+features_name+'_model.h5')
    df = load_data(features_name, meeting_date, drop_columns)
    races = df.groupby('race_number')
    df = df.drop(columns=['race_number'])
    for race_number, horses in races:
        horses = horses.drop(columns=['race_number'])

        for _, row in horses.iterrows():
            horse_number = int(row['number'])
            test_x = row.values.reshape(1, row.shape[0])
            p = model.predict(test_x)
            p = p[0][0]
            query = Entries.update(prob=p).where((Entries.meeting_date == meeting_date) &
                                                 (Entries.race_number == race_number) &
                                                 (Entries.number == horse_number))
            query.execute()


def export_predict_table(meeting_date):
    entries = Entries.select().where(
        Entries.meeting_date == meeting_date
    ).order_by(Entries.race_number, Entries.number)

    filename = 'data/predict/predict_'+str(meeting_date.year)+'-'+('%02d' %
                                                                   (meeting_date.month))+'-'+('%02d' % (meeting_date.day))+'.csv'
    f = open(filename, 'w+')
    f.write('\ufeff')  # UTF-8 BOM
    f.write("date,race_number,馬號,馬名,騎師,練馬師,檔位,負磅,排位體重,獨贏,位置,prob\r\n")
    for entry in entries:
        f.write("%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\r\n" % (entry.meeting_date, entry.race_number, entry.number, entry.horse_name, entry.jockey_name,
                                                             entry.trainer_name, entry.draw, entry.actual_weight, entry.declared_weight, entry.win_odds, entry.place_odds, entry.prob))
    print('File exported %s' % filename)
