'''
    this is a tool to generate data in features table
    1. first, auto-gen the tables definition
    > db.sh
    2. table meetings.meeting_date needs to be updated
    > python prepare.py --task prepare_meetings
    3. now we can generate the features for machine learning
    > python prepare.py --task generate_features
    4. use DB tool(e.g. Navicat) to export train/test CSV based on date
    > "select * from features1 where meeting_date < '2018-08-01' order by id" > data/features1_train.csv
    > "select * from features1 where meeting_date > '2018-08-01' order by id" > data/features1_test.csv
'''
# -*- coding: utf-8 -*-
from __future__ import division

import sys
import csv
import os
import pprint
import traceback
import re

import numpy as np
import peewee
from datetime import datetime, timedelta
import operator
from functools import reduce

from db import *

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
pp = pprint.PrettyPrinter(depth=6)


def parse_distance(distance):
    r = None
    match = re.search(r"(\d{4})米", distance)
    if match:
        r = match.group(1)
    return r


def label_course(course):
    r = None
    if "全天候跑道" in course:
        r = 1
    if "A" in course:
        r = 2
    if "B" in course:
        r = 3
    if "C" in course:
        r = 4
    return r


def label_location(location):
    r = None
    if "沙田" in location:
        r = 1
    if "跑馬地" in location:
        r = 2
    return r


def label_new_horse_race(horses_class):
    r = 0
    if type(horses_class) is str:
        if "新馬賽" in horses_class:
            r = 1
    return r


def parse_running_time_to_centisecond(running_time):
    # time in format 2:33.09 or 2.33.09
    running_time = running_time.replace(':', '.')
    r = None
    match = re.search(r"(\d{1,}).(\d{1,}).(\d{1,})", running_time)
    if match:
        minute = int(match.group(1)) * 60 * 100
        second = int(match.group(2)) * 100
        centisecond = int(match.group(3))
        r = minute + second + centisecond

    return r

# HKJC has different code format for the same horse
# this function find the list of horse_ids with same horse code by a given horse_id


def find_duplicate_horse_ids(horse_id):
    horse = Horses.select().where(Horses.id == horse_id).get()
    # V202
    short_code_format = re.search(r"^([a-zA-Z]{1}\d{3,})", horse.code)
    # HK_2015_V202
    long_code_format = re.search(r"[_]([a-zA-Z]{1}\d{3,})$", horse.code)

    r = [horse_id]
    if short_code_format:
        horses = Horses.select().where(Horses.code.endswith('_'+short_code_format.group(1)))
    if long_code_format:
        horses = Horses.select().where(Horses.code == long_code_format.group(1))

    for horse in horses:
        if horse.id not in r:
            r.append(horse.id)

    return r


def get_previous_race(before_meeting_date, horse_id):
    horses_ids = find_duplicate_horse_ids(horse_id)
    horses = []
    for x in horses_ids:
        horses.append((Results.horse_id == x))

    conditions = [
        (Results.place > 0),
        (reduce(operator.or_, horses))
    ]

    r = None
    try:
        result = Results.select(Results, Meetings.meeting_date).where(
            reduce(operator.and_, conditions)
        ).join(
            Races, on=(Results.race_id == Races.id)
        ).switch(Results).join(
            Meetings, on=(Races.meeting_id == Meetings.id)
        ).where(
            Meetings.datevalue.startswith('Local') &
            (Meetings.meeting_date < before_meeting_date)
        ).order_by(Meetings.meeting_date.desc()).get()
        race = Races.select().where(Races.id == result.race_id).get()
        r = {'result': result, 'race': race}
    except Results.DoesNotExist:
        print("No previous race for horse %s before %s" %
              (horse_id, before_meeting_date))

    return r


def find_race_result_before(before_meeting_date, conditions):
    played = 0
    won = 0
    first_three = 0

    # performance from 3 years ago
    after_meeting_date = str((before_meeting_date.year - 3)) + '-' + \
        str(before_meeting_date.month) + '-' + str(before_meeting_date.day)
    after_meeting_date = datetime.strptime(after_meeting_date, '%Y-%m-%d')

    results = Results.select().where(
        reduce(operator.and_, conditions)
    ).join(
        Races, on=(Results.race_id == Races.id)
    ).join(
        Meetings, on=(Races.meeting_id == Meetings.id)
    ).where(
        Meetings.datevalue.startswith('Local') &
        (Meetings.meeting_date < before_meeting_date) &
        (Meetings.meeting_date > after_meeting_date)
    ).order_by(Meetings.meeting_date)

    for result in results:
        played = played + 1
        if result.place == 1:
            won = won + 1
        if result.place <= 3:
            first_three = first_three + 1
    r = (played, won, first_three)

    return r


def find_horse_jockey_race_result(before_meeting_date, horse_id, jockey_id):
    horses_ids = find_duplicate_horse_ids(horse_id)
    horses = []
    for x in horses_ids:
        horses.append((Results.horse_id == x))

    conditions = [
        (Results.place > 0),
        (reduce(operator.or_, horses)),
        (Results.jockey_id == jockey_id)
    ]

    return find_race_result_before(before_meeting_date, conditions)


def find_horse_race_result(before_meeting_date, horse_id):
    horses_ids = find_duplicate_horse_ids(horse_id)
    horses = []
    for x in horses_ids:
        horses.append((Results.horse_id == x))

    conditions = [
        (Results.place > 0),
        (reduce(operator.or_, horses))
    ]

    return find_race_result_before(before_meeting_date, conditions)


def find_horse_race_same_distance_result(before_meeting_date, horse_id, distance):
    horses_ids = find_duplicate_horse_ids(horse_id)
    horses = []
    for x in horses_ids:
        horses.append((Results.horse_id == x))

    conditions = [
        (Results.place > 0),
        (reduce(operator.or_, horses)),
        (Races.distance.contains(distance))
    ]

    return find_race_result_before(before_meeting_date, conditions)


def find_horse_class_adjustment(horse_id, before_meeting_date, current_horses_class):
    horses_ids = find_duplicate_horse_ids(horse_id)
    horses = []
    for x in horses_ids:
        horses.append((Results.horse_id == x))

    conditions = [
        (Results.place > 0),
        (reduce(operator.or_, horses))
    ]

    r = 0  # same

    try:
        results = (Meetings.select(Races.horses_class).where(
            Meetings.datevalue.startswith('Local') &
            (Meetings.meeting_date < before_meeting_date)
        ).switch(Meetings).join(
            Races, on=(Meetings.id == Races.meeting_id)
        ).join(
            Results, on=(Races.id == Results.race_id)
        ).where(
            reduce(operator.and_, conditions)
        ).order_by(Meetings.meeting_date.desc())).get()
        horses_class = results.races.horses_class

        if (current_horses_class != horses_class) and (current_horses_class is not None) and (horses_class is not None):
            current_hc = 0
            hc = 0
            if '一' in current_horses_class:
                current_hc = 1
            if '二' in current_horses_class:
                current_hc = 2
            if '三' in current_horses_class:
                current_hc = 3
            if '四' in current_horses_class:
                current_hc = 4
            if '五' in current_horses_class:
                current_hc = 5
            if '一' in horses_class:
                hc = 1
            if '二' in horses_class:
                hc = 2
            if '三' in horses_class:
                hc = 3
            if '四' in horses_class:
                hc = 4
            if '五' in horses_class:
                hc = 5
            if current_hc > 0 and hc > 0:
                if current_hc < hc:
                    r = 1  # up class
                if current_hc > hc:
                    r = 2  # down class
    except Meetings.DoesNotExist:
        print("No previous race for horse %s before %s" %
              (horse_id, before_meeting_date))

    return r


def find_days_since_last_race(horse_id, before_meeting_date):
    horses_ids = find_duplicate_horse_ids(horse_id)
    horses = []
    for x in horses_ids:
        horses.append((Results.horse_id == x))

    conditions = [
        (Results.place > 0),
        (reduce(operator.or_, horses))
    ]

    r = 0

    try:
        results = (Meetings.select().where(
            Meetings.datevalue.startswith('Local') &
            (Meetings.meeting_date < before_meeting_date)
        ).join(
            Races, on=(Meetings.id == Races.meeting_id)
        ).join(
            Results, on=(Races.id == Results.race_id)
        ).where(
            reduce(operator.and_, conditions)
        ).order_by(Meetings.meeting_date.desc())).get()
        delta = before_meeting_date - results.meeting_date
        r = delta.days
    except Meetings.DoesNotExist:
        print("No previous race for horse %s before %s" %
              (horse_id, before_meeting_date))

    return r


def get_horse_code(horse_id):
    r = None
    horse = Horses.select().where(Horses.id == horse_id).get()
    # V202
    short_code_format = re.search(r"^([a-zA-Z]{1}\d{3,})", horse.code)
    # HK_2015_V202
    long_code_format = re.search(r"[_]([a-zA-Z]{1}\d{3,})$", horse.code)

    if short_code_format:
        r = short_code_format.group(1)
    if long_code_format:
        r = long_code_format.group(1)

    return r


def get_jockey_code(jockey_id):
    r = None
    jockey = Jockeys.select().where(Jockeys.id == jockey_id).get()
    r = jockey.code

    return r


def get_trainer_code(trainer_id):
    r = None
    trainer = Trainers.select().where(Trainers.id == trainer_id).get()
    r = trainer.code

    return r


def fix_rating_adjustment():
    entries = Entries.select().where(
        Entries.rating_adjustment_value == None).order_by(Entries.id)
    for i in entries:
        rating_adjustment = 0
        try:
            rating = i.rating
            rating = int(rating)
            rating_adjustment = get_rating_adjustment(
                i.horse_id, i.meeting_date, rating)
        except ValueError:
            rating_adjustment = 0
        i.rating_adjustment_value = rating_adjustment
        i.save()


# calculate the rating diff from previous race
def get_rating_adjustment(horse_id, meeting_date, current_rating):
    rating_original = current_rating
    horse_code = get_horse_code(horse_id)
    horses = Horses.select().where(Horses.code.contains(horse_code))

    previous_date = None
    for i in horses:
        entries = Entries.select().where(
            (Entries.meeting_date < meeting_date) &
            (Entries.horse_id == i.id)
        ).order_by(Entries.meeting_date.desc())
        for j in entries:
            if previous_date is None or previous_date < j.meeting_date:
                rating_original = int(j.rating)
                previous_date = j.meeting_date
            break

    rating_adjustment = current_rating - rating_original

    return rating_adjustment


def get_entry_rating(meeting_date, race_number):
    entries = Entries.select().where(
        (Entries.meeting_date == meeting_date) &
        (Entries.race_number == race_number)
    ).order_by(Entries.number)

    highest_rating = 0
    highest_rating_original = 0
    r_rating = {}
    r_rating_original = {}
    r_rating_adjusted_rate = {}
    r_rating_relative = {}
    r_rating_original_relative = {}
    valid = True

    for entry in entries:
        try:
            rating = entry.rating
            rating = int(rating)
            # entry.rating_adjustment from horse scrpe is wrong, HKJC shows horse newest rating for all past races
            # see fix_rating_adjustment()
            rating_adjustment = entry.rating_adjustment_value
            rating_original = rating + (rating_adjustment * -1)
            if rating == 0:
                raise ValueError('Zero rating')
            if rating > highest_rating:
                highest_rating = rating
            if rating_original > highest_rating_original:
                highest_rating_original = rating_original
        except ValueError:
            valid = False

    r = None
    if valid == True:
        rating_original_list = []
        for entry in entries:
            number = str(entry.number)
            rating = int(entry.rating)
            rating_adjustment = entry.rating_adjustment_value
            rating_original = rating + (rating_adjustment * -1)
            r_rating[number] = rating
            r_rating_original[number] = rating_original
            if rating_original == 0:
                r_rating_adjusted_rate[number] = 0
            else:
                r_rating_adjusted_rate[number] = rating / rating_original
            r_rating_relative[number] = rating / highest_rating
            r_rating_original_relative[number] = rating_original / \
                highest_rating_original
            rating_original_list.append(rating_original)
        r_rating_original_variance = np.var(rating_original_list).item()
        r = [r_rating, r_rating_original, r_rating_adjusted_rate,
             r_rating_relative, r_rating_original_relative, r_rating_original_variance]

    return r


def prepare_meetings():
    meetings = Meetings.select().where(
        Meetings.datevalue.startswith('Local%')
    ).order_by(Meetings.datevalue)
    c = 0
    for meeting in meetings:
        match = re.search(r"(\d{8})", meeting.datevalue)
        if match:
            result = match.group(1)
            meeting_date = '%s-%s-%s' % (result[:4], result[4:6], result[6:8])
            query = Meetings.update(meeting_date=meeting_date).where(
                Meetings.id == meeting.id
            )
            query.execute()
            c = c + 1
    print('Rows updated %s' % c)
