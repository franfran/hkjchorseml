# -*- coding: utf-8 -*-
import numpy as np
import pandas as pd
import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout, LeakyReLU
from keras.optimizers import Adam
from keras.layers.normalization import BatchNormalization
from sklearn.preprocessing import MinMaxScaler, LabelEncoder
from sklearn.externals import joblib

# random seed
seed = 7
np.random.seed(seed)


# todo re-factor
def load_data_with_scalar():
    df = pd.read_csv('data/features1_all.csv')
    df = df[df.new_horse_race == 0]
    horseencoder = LabelEncoder()
    jockeyencoder = LabelEncoder()
    trainerencoder = LabelEncoder()
    horseencoder.fit_transform(df['horse_code'])
    jockeyencoder.fit_transform(df['jockey_code'])
    trainerencoder.fit_transform(df['trainer_code'])

    df = pd.read_csv('data/features1_train.csv')
    df = df[df.new_horse_race == 0]
    train_x = df.drop(
        columns=['id', 'race_id', 'result_id', 'meeting_date', 'result_first_three', 'result_won', 'new_horse_race', 'number_of_horses'])
    train_y = df[['result_first_three']]

    df = pd.read_csv('data/features1_test.csv')
    df = df[df.new_horse_race == 0]
    test_x = df.drop(
        columns=['id', 'race_id', 'result_id', 'meeting_date', 'result_first_three', 'result_won', 'new_horse_race', 'number_of_horses'])
    test_y = df[['result_first_three']]

    train_x['horse_code'] = horseencoder.transform(train_x['horse_code'])
    train_x['jockey_code'] = jockeyencoder.transform(train_x['jockey_code'])
    train_x['trainer_code'] = trainerencoder.transform(train_x['trainer_code'])
    test_x['horse_code'] = horseencoder.transform(test_x['horse_code'])
    test_x['jockey_code'] = jockeyencoder.transform(test_x['jockey_code'])
    test_x['trainer_code'] = trainerencoder.transform(test_x['trainer_code'])
    joblib.dump(horseencoder, 'data/features1_horseencoder.pkl')
    joblib.dump(jockeyencoder, 'data/features1_jockeyencoder.pkl')
    joblib.dump(trainerencoder, 'data/features1_trainerencoder.pkl')

    scaler = MinMaxScaler()
    train_x = scaler.fit_transform(train_x)
    test_x = scaler.transform(test_x)
    joblib.dump(scaler, 'data/features1_scaler.pkl')

    return (train_x, train_y, test_x, test_y)


def load_data(features_name, drop_columns, column_y):
    df = pd.read_csv('data/'+features_name+'_train.csv')
    df = df[df.new_horse_race == 0]
    train_x = df.drop(columns=drop_columns)
    train_y = df[[column_y]]

    df = pd.read_csv('data/'+features_name+'_test.csv')
    df = df[df.new_horse_race == 0]
    test_x = df.drop(columns=drop_columns)
    test_y = df[[column_y]]

    return (train_x, train_y, test_x, test_y)


def build_model(input_dim):
    optimizer = Adam(lr=0.001)
    model = Sequential()
    #model.add(Dense(1024, input_dim=input_dim, activation='relu'))
    model.add(Dense(1024, input_dim=input_dim))
    model.add(LeakyReLU(alpha=0.05))
    model.add(BatchNormalization())  # prevent blowing
    model.add(Dropout(0.3))

    #model.add(Dense(512, activation='relu'))
    model.add(Dense(512))
    model.add(LeakyReLU(alpha=0.05))
    model.add(BatchNormalization())  # prevent blowing
    model.add(Dropout(0.3))

    #model.add(Dense(256, activation='relu'))
    model.add(Dense(256))
    model.add(LeakyReLU(alpha=0.05))
    model.add(BatchNormalization())  # prevent blowing
    model.add(Dropout(0.3))

    model.add(Dense(units=1, activation='sigmoid'))

    model.compile(loss='binary_crossentropy',
                  optimizer=optimizer, metrics=['accuracy'])

    return model
