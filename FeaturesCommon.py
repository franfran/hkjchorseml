# -*- coding: utf-8 -*-
from __future__ import division

import argparse
import sys
import os
import traceback
import re
from datetime import datetime
from matplotlib import pyplot
from db import *

from prepare import get_entry_rating, prepare_meetings, fix_rating_adjustment
from prepare import parse_distance, label_course, label_location, label_new_horse_race
from train import load_data, build_model
from predict import predict, export_predict_table
from simulate import simulate


class FeaturesCommon(object):
    db_features = None
    db_entriesfeatures = None
    features_name = None
    drop_columns = None
    training_epochs = 40
    column_y = 'result_first_three'
    #column_y = 'result_won'

    def prepare_features(self, row, meeting_date, distance, course, location, number_of_horses, horses_class, entry_rating):
        data = {}

        return data

    def generate_entriesfeatures(self, meeting_date):
        self.db_entriesfeatures.truncate_table()
        entries = Entries.select().where(
            Entries.meeting_date == meeting_date
        ).order_by(Entries.race_number)

        c = 0
        for entry in entries:
            race_number = entry.race_number
            entry_rating = get_entry_rating(meeting_date, race_number)
            if entry_rating is None:
                print("Meeting %s Race %s has no entry rating" %
                      (meeting_date, race_number))
                continue

            data = self.prepare_features(entry, meeting_date, entry.distance, entry.course,
                                         entry.location, entry.number_of_horses, entry.horses_class, entry_rating)
            data.update({
                'race_id': 0, 'result_id': 0, 'race_number': race_number,
                'result_first_three': 0, 'result_won': 0
            })
            data = [data]

            query = self.db_entriesfeatures.insert_many(data)
            query.execute()
            c = c + 1
        print('Rows inserted %s' % c)

    def generate_features(self):
        meetings = Meetings.select().where(
            Meetings.datevalue.startswith('Local%')
        ).order_by(Meetings.meeting_date)

        c = 0
        for meeting in meetings:
            races = Races.select().where(
                Races.meeting_id == meeting.id
            ).order_by(Races.race_number)
            for race in races:
                cancel = re.search(r"(退款)", race.report)
                if cancel:
                    print("Meeting %s Race %s 退款" % (meeting.id, race.id))
                    continue
                entry_rating = get_entry_rating(
                    meeting.meeting_date, race.race_number)
                if entry_rating is None:
                    print("Meeting %s Race %s has no entry rating" %
                          (meeting.id, race.id))
                    continue
                number_of_horses = race.number_of_horses
                distance = parse_distance(race.distance)
                course = label_course(race.course)
                location = label_location(race.location)
                if None in (number_of_horses, distance, course, location):
                    print("Meeting %s Race %s data incomplete" %
                          (meeting.id, race.id))
                    continue
                results = Results.select().where(
                    Results.race_id == race.id
                ).order_by(Results.place)
                for result in results:
                    if result.place == 0:
                        print("Meeting %s Race %s Result %s 退出" %
                              (meeting.id, race.id, result.id))
                        continue
                    if str(result.number) not in entry_rating[0]:
                        print("Meeting %s Race %s Result %s Number %s has no entry" %
                              (meeting.id, race.id, result.id, result.number))
                        continue
                    exist = self.db_features.select().where(
                        self.db_features.result_id == result.id).count()
                    if exist == 0:
                        result_first_three = 0
                        result_won = 0
                        if result.place <= 3:
                            result_first_three = 1
                        if result.place == 1:
                            result_won = 1

                        data = self.prepare_features(result, meeting.meeting_date, race.distance, race.course,
                                                     race.location, number_of_horses, race.horses_class, entry_rating)
                        data.update({
                            'race_id': race.id, 'result_id': result.id,
                            'result_first_three': result_first_three, 'result_won': result_won
                        })
                        data = [data]

                        query = self.db_features.insert_many(data)
                        query.execute()
                        c = c + 1
        print('Rows inserted %s' % c)

    # Rank the best features using SelectKBest class
    def bestfeatures(self):
        import pandas as pd
        from sklearn.feature_selection import SelectKBest
        from sklearn.feature_selection import chi2

        number_of_best = 20

        df = pd.read_csv('data/'+self.features_name+'_train.csv')
        X = df.drop(columns=self.drop_columns)
        Y = df[[self.column_y]]

        bestfeatures = SelectKBest(score_func=chi2, k=number_of_best)
        fit = bestfeatures.fit(X, Y)
        dfscores = pd.DataFrame(fit.scores_)
        dfcolumns = pd.DataFrame(X.columns)

        # concat two dataframes for better visualization
        featureScores = pd.concat([dfcolumns, dfscores], axis=1)
        featureScores.columns = ['Specs', 'Score']
        print(featureScores.nlargest(number_of_best, 'Score'))

    def train(self):
        (train_x, train_y, test_x, test_y) = load_data(
            self.features_name, self.drop_columns, self.column_y)
        model = build_model(train_x.shape[1])
        history = model.fit(train_x, train_y, epochs=self.training_epochs,
                            validation_data=(test_x, test_y), shuffle=False, batch_size=512)

        # plot train and validation loss
        pyplot.plot(history.history['loss'])
        pyplot.plot(history.history['val_loss'])
        pyplot.title('model train vs validation loss')
        pyplot.ylabel('loss')
        pyplot.xlabel('epoch')
        pyplot.legend(['train', 'validation'], loc='upper right')
        pyplot.show()

        model.save('data/'+self.features_name+'_model.h5')

    def main(self, args=None):
        parser = argparse.ArgumentParser(
            prog=self.getExec(), description='Prepare features for learning')
        parser.add_argument(
            '--task',
            help='Job task: prepare_meetings, generate_features, generate_entriesfeatures'
        )
        parser.add_argument(
            '--date',
            help='date for generate_entriesfeatures, e.g. "2019-02-29"'
        )
        options = parser.parse_args(args)

        if options.task == "prepare_meetings":
            prepare_meetings()
        if options.task == "generate_features":
            fix_rating_adjustment()
            self.generate_features()
        if options.task == "bestfeatures":
            fix_rating_adjustment()
            self.bestfeatures()
        if options.task == "train":
            self.train()
        if options.task == "simulate":
            simulate(self.features_name, self.drop_columns, self.column_y)
        if options.task == "predict":
            meeting_date = datetime.strptime(options.date, '%Y-%m-%d')
            self.generate_entriesfeatures(meeting_date)
            predict(self.features_name, meeting_date, self.drop_columns)
            export_predict_table(meeting_date)

    def getExec(self):
        try:
            f = os.path.abspath(sys.modules['__main__'].__file__)
        except:
            f = sys.executable
        return os.path.basename(f)

    def run(self):
        if __name__ == '__main__':
            rc = 1
            try:
                self.main()
                rc = 0
            except Exception as e:
                print('Error: %s' % e)
                print(traceback.format_exc())
            sys.exit(rc)
