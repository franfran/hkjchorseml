"""
    Load trained model(data/model.h5) and simulate the betting of data/features1_test.csv
    It will use the class Dividend to load payout result
"""

# -*- coding: utf-8 -*-
import re
import numpy as np
import pandas as pd
import itertools
from random import shuffle, random
from keras.models import load_model
from sklearn.preprocessing import MinMaxScaler
from sklearn.externals import joblib
from dividend import Dividend
from pprint import pprint
from statistics import mean
from math import floor

# settings
# minimum is 3 legs
setting_bet_legs = 3
setting_full_mode = False
setting_bet_cost = 10 #$10 dollars per bet
setting_bet_principal = 1000

# dutching
setting_bet_dutching =  False
# balance could become negative if setting_bet_kelly is False
# set setting_bet_dutching_ignore_balance = True, if you want to simulate on fixed betting with dutching
setting_bet_dutching_ignore_balance = True

# kelly criterion
setting_bet_kelly = True
#Run the simulate once to get the probability value, we have only implemented 'win' pool for kelly bet so far
setting_bet_kelly_win_probability = 0.2
#percentage to bet on each kelly estimate, the higher of this number, the chance of money balance burst is higher
setting_bet_kelly_fraction = 0.3

# In hot odds case scenario, all winnings are hottest in day meeting(public are accurate).  So even our predictions are accurate, we still lose money
# one possible way to solve it, is bet 'much' more on hot odds.
# It is a bit conflict with kelly, since the return could be less and why bet more?
# Since the public has 70% accuracy on hot odds, it is quite safe to bet more on hot odds.
# However, after multiple round of testing and verification, 'hotpush' method won't work.
# It is because we bet more money and exposed the risk, but kelly handled risk already
setting_hotpush = False
setting_hotpush_factor = 2

# due to 落飛黨, the final odds could be much lower, use this setting to simulate
# becareful this setting can increase the bet amount(due to kelly) so risk will become higher
setting_delay_hotodds = False
setting_delay_hotodds_factor = 2

# Dutching can help to reduce the money lost since the bet amount are spread unevenly.
# However, it also reduce the return if one of the odds in bet list has high return.
# Actually, kelly has included the risk control and bet on highest return already.
# Can we have the best of both?
# e.g. in a race, if all odds are mid/high(5.6/7.8/20), we don't use Dutching.  So all bet amount are the same
# and for another race, if one of the odds is very low(1.8/7.8/20), we use Dutching.  Assuming the odds 1.8 will win.
setting_bet_dutching_mixed = False
if setting_bet_dutching_mixed:
    setting_bet_kelly = True
    setting_bet_dutching = False

setting_write_bet_details = False
setting_write_bet_details_pool = 'win'

# we can bet only if expected return is higher than win_odds
# in fact the total return are roughly the same between "more bet with hot win_odds" vs "less bet with cold win_odds"
# set setting_bet_higher_expected_return_only to True if want to simulate "less bet with cold win_odds"
setting_bet_higher_expected_return_only = False

# bet amount limit
# 每注最高金額港幣50,000元
# 投注戶口每受注日最高投注金額為港幣500,000元
setting_max_bet_per_race = 50000

# to get the 'safe' range for predicted variance, 1. enable print_trio_stats(), 2. enable setting_use_predicted_variance
setting_use_predicted_variance = False
setting_use_predicted_variance_min = 4
setting_use_predicted_variance_max = 16.5


bet_principal_accumulated = setting_bet_principal

# counter variables
bet_legs_won = 0
number_of_races = 0

lost_last_two = 0
highest_draw_last_two_won = 0
lowest_draw_first_two_won = 0

distance_count = {}
location_count = {}

trio_success_variance_for_rating_original = []
trio_failed_variance_for_rating_original = []
trio_success_variance_for_predicted = []
trio_failed_variance_for_predicted = []

# 獨贏(win), 位置(place), 連贏(quinella), 位置Q(quinella_place), 單T(trio), 三重彩(tierce), 四連環(first_4), 四重彩(quartet), 孖T(double_trio), 三T(triple_trio)
bet_count = {'win': {}, 'place': {}, 'quinella': {}, 'quinella_place': {},
            'trio': {}, 'tierce': {}, 'first_4': {}, 'quartet': {},
            'double_trio': {}, 'triple_trio': {}}

# todo re-factor
def load_data_with_scalar():
    df = pd.read_csv('data/features1_test.csv')
    df = df[df.new_horse_race == 0]
    df = df.drop(
        columns=['id', 'result_id', 'meeting_date', 'result_won', 'new_horse_race', 'number_of_horses'])

    scaler = joblib.load('data/features1_scaler.pkl')
    horseencoder = joblib.load('data/features1_horseencoder.pkl')
    jockeyencoder = joblib.load('data/features1_jockeyencoder.pkl')
    trainerencoder = joblib.load('data/features1_trainerencoder.pkl')

    df['horse_code'] = horseencoder.transform(df['horse_code'])
    df['jockey_code'] = jockeyencoder.transform(df['jockey_code'])
    df['trainer_code'] = trainerencoder.transform(df['trainer_code'])

    df = scaler.transform(df)

    return df


def load_data(features_name):
    df = pd.read_csv('data/'+features_name+'_test.csv')
    df = df[df.new_horse_race == 0]

    return df


def five_number_summary(data_list):
    quartiles = np.percentile(data_list, [25, 50, 75])
    data_min, data_max = min(data_list), max(data_list)

    # (Min Q1 Median Q3 Max)
    return (data_min, quartiles[0], quartiles[1], quartiles[2], data_max)


def print_trio_stats():
    print('單T Five Number Summary for success variance: %s' %
          (five_number_summary(trio_success_variance_for_rating_original),))
    print('單T Mean for success variance: %s' %
          mean(trio_success_variance_for_rating_original))
    print('單T Five Number Summary for failed variance: %s' %
          (five_number_summary(trio_failed_variance_for_rating_original),))
    print('單T Mean for failed variance: %s' %
          mean(trio_failed_variance_for_rating_original))
    print('單T Five Number Summary for trio_success_variance_for_predicted: %s' %
          (five_number_summary(trio_success_variance_for_predicted),))
    print('單T Mean for trio_success_variance_for_predicted: %s' %
          mean(trio_success_variance_for_predicted))
    print('單T Five Number Summary for trio_failed_variance_for_predicted: %s' %
          (five_number_summary(trio_failed_variance_for_predicted),))
    print('單T Mean for trio_failed_variance_for_predicted: %s' %
          mean(trio_failed_variance_for_predicted))


def bet_count_increase(pool, meeting_date, bet_list, bet_result, distance, split_bet = {}):
    global bet_count, bet_principal_accumulated

    meeting_month = re.search(r"^\d{4}\/(\d{2})\/\d{2}", meeting_date)
    meeting_month = int(meeting_month.group(1))
    if meeting_date not in bet_count[pool]['date']:
        bet_count[pool]['date'][meeting_date] = {'bet':0, 'won':0, 'prize':0, 'split_bet':0, 'split_bet_prize':0, 'balance':0, 'hotwon':0}
    bet_count[pool]['date'][meeting_date]['bet'] = bet_count[pool]['date'][meeting_date]['bet'] + len(bet_list)
    bet_count[pool][meeting_month]['bet'] = bet_count[pool][meeting_month]['bet'] + \
        len(bet_list)
    split_bet_total_for_this_race = 0
    split_bet_prize_total_for_this_race = 0
    if len(split_bet) > 0:
        for i in split_bet:
            split_bet_total_for_this_race = split_bet_total_for_this_race + split_bet[i]['bet']
            bet_count[pool]['date'][meeting_date]['split_bet'] = bet_count[pool]['date'][meeting_date]['split_bet'] + split_bet[i]['bet']
    if bet_result is not None:
        for i in bet_result:
            bet_count[pool]['distance'][distance] = bet_count[pool]['distance'][distance] + 1
            bet_count[pool][meeting_month]['won'] = bet_count[pool][meeting_month]['won'] + 1
            bet_count[pool][meeting_month]['prize'] = bet_count[pool][meeting_month]['prize'] + bet_result[i]
            bet_count[pool]['date'][meeting_date]['won'] = bet_count[pool]['date'][meeting_date]['won'] + 1
            bet_count[pool]['date'][meeting_date]['prize'] = bet_count[pool]['date'][meeting_date]['prize'] + bet_result[i]
            if len(split_bet) > 0:
                # hotwon counter can check if the odds is so low that the return not justify.  Only applies to pool 'win'
                if (split_bet[int(i)]['odds']) < len(split_bet):
                    bet_count[pool]['date'][meeting_date]['hotwon'] = bet_count[pool]['date'][meeting_date]['hotwon'] + 1
                split_bet_prize_total_for_this_race = split_bet_prize_total_for_this_race + (split_bet[int(i)]['bet'] * split_bet[int(i)]['odds'])
                bet_count[pool]['date'][meeting_date]['split_bet_prize'] = bet_count[pool]['date'][meeting_date]['split_bet_prize'] + (split_bet[int(i)]['bet'] * split_bet[int(i)]['odds'])
    if len(split_bet) > 0:
        bet_principal_accumulated = (bet_principal_accumulated - split_bet_total_for_this_race) + split_bet_prize_total_for_this_race
        bet_count[pool]['date'][meeting_date]['balance'] = bet_principal_accumulated


def bet_count_total(pool):
    b = 0
    w = 0
    p = 0

    for i in range(1, 13):
        b = b + bet_count[pool][i]['bet']
        w = w + bet_count[pool][i]['won']
        p = p + bet_count[pool][i]['prize']

    return {'bet': b, 'won': w, 'prize': p}


def write_day_stats():
    for i in bet_count:
        if setting_bet_legs < 3:
            if (i == 'trio') or (i == 'tierce') or (i == 'first_4') or (i == 'quartet') or (i == 'double_trio'):
                continue
        filename = 'data/simulate/day_stats_'+i+'.csv'
        f= open(filename,'w+')
        f.write("date,bet,won,prize,rate,bet total,earning,split bet,split bet prize,split bet earning,balance,hotwon\r\n")
        for j in bet_count[i]['date']:
            bet = bet_count[i]['date'][j]['bet']
            won = bet_count[i]['date'][j]['won']
            prize = bet_count[i]['date'][j]['prize']
            rate = won/bet
            bet_total = bet * setting_bet_cost
            earning = prize - bet_total
            split_bet = bet_count[i]['date'][j]['split_bet']
            split_bet_prize = bet_count[i]['date'][j]['split_bet_prize']
            split_bet_earning = split_bet_prize - split_bet
            balance = bet_count[i]['date'][j]['balance']
            hotwon = bet_count[i]['date'][j]['hotwon']
            f.write("%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\r\n" % (j, bet, won, prize, rate, bet_total, earning, split_bet, split_bet_prize, split_bet_earning, balance, hotwon))


"""
    bet_total = total bet amount
    bet_list = {horse_number: odds}
    e.g. bet_list = {1: 6, 2: 1.7, 3: 17}
    get_dutching_amount(bet_total, bet_list)
"""
def get_dutching_amount(bet_total, odds_list):
    product = 1
    bet_ratio = {}
    bet_sum = 0
    r = {}
    for i in odds_list:
        product = product * odds_list[i]
    for i in odds_list:
        bet_ratio[i] = product/odds_list[i]
    for i in odds_list:
        bet_sum = bet_sum + bet_ratio[i]
    for i in odds_list:
        # round to nearest 10 for the min. bet of $10
        r[i] = {}
        r[i]['odds'] = odds_list[i]
        r[i]['bet'] = round((bet_total / bet_sum * bet_ratio[i]), -1)

    return r

def get_kelly_amount(bet_principal, odds, winning_probability):
    r = bet_principal * \
        (odds - (1/winning_probability)) * (winning_probability/(odds-1))
    r = round(r, -1)

    return r

def simulate(features_name, drop_columns, column_y):
    global bet_legs_won, number_of_races
    global lost_last_two, highest_draw_last_two_won, lowest_draw_first_two_won
    global distance_count, location_count
    global trio_success_variance_for_rating_original, trio_failed_variance_for_rating_original
    global trio_success_variance_for_predicted, trio_failed_variance_for_predicted
    global bet_count

    model = load_model('data/'+features_name+'_model.h5')

    # we need to keep these columns for simulation
    drop_columns.remove('race_id')
    drop_columns.remove('meeting_date')
    drop_columns.remove(column_y)

    df_ori = load_data(features_name)
    df = df_ori.drop(columns=drop_columns)

    races = df.groupby('race_id')
    df = df.drop(columns=['race_id'])

    for i in bet_count:
        bet_count[i]['date'] = {}
        bet_count[i]['distance'] = {'1000': 0, '1200': 0, '1400': 0,
                                    '1600': 0, '1650': 0, '1800': 0,
                                    '2000': 0, '2200': 0, '2400': 0}
        for j in range(1, 13):
            bet_count[i][j] = {'bet': 0, 'won': 0, 'prize': 0}


    if setting_write_bet_details:
        f_bet_details = open('data/simulate/bet_details_'+setting_write_bet_details_pool+'.csv','w+')
        f_bet_details.write("date,race_number,horse_number,odds,bet,won\r\n")

    # simulate start
    meeting_date = None
    bet_list_today = {}
    race_number = 1
    for race_id, race in races:
        if meeting_date != race.meeting_date.tolist()[0]:
            meeting_date = str(race.meeting_date.tolist()[0])
            bet_list_today = {}
            race_number = 1

        dividend = Dividend(race_id)
        race_number = dividend.get_race_number()

        bet_list_today[race_number] = {}

        meeting_month = df_ori.loc[df_ori['race_id'] == race_id]
        meeting_month = meeting_month['meeting_date'].iloc[0]
        meeting_month = meeting_month.split('/')[1]
        meeting_month = int(meeting_month)
        horses = race
        horses = horses.drop(
            columns=['race_id', 'meeting_date', column_y])
        horses_distance = horses.distance.tolist()
        horses_distance = str(horses_distance[0])
        horses_location = horses.location.tolist()
        horses_location = str(horses_location[0])
        if horses_distance not in distance_count:
            distance_count[horses_distance] = 0
        if horses_location not in location_count:
            location_count[horses_location] = 0

        results_first_three = []
        random_first_three = []
        predicted_list = {}
        draw_list = {}
        rating_original_list = []


        for index, row in horses.iterrows():
            rating_original_list.append(row['rating_original'])
            horse_number = df_ori.iloc[[index]]
            horse_number = horse_number.iloc[0]['number']
            horse_number = int(horse_number)
            test_x = row.values.reshape(1, row.shape[0])
            test_y = race.loc[[index]][[column_y]].iloc[0][column_y]

            p = model.predict(test_x)
            predicted_list[horse_number] = p[0][0]
            draw_list[horse_number] = int(row['draw'])
            random_first_three.append(horse_number)
            if test_y == 1:
                results_first_three.append(horse_number)

        # to avoid having 100% accuracy due to results are sorted from first place in data source
        l = list(predicted_list.items())
        shuffle(l)
        predicted_list = dict(l)

        # now we sort them and get the running horses
        psorted = sorted(predicted_list.items(), key=lambda x: x[1], reverse=True)
        predicted_horses = []
        predicted_horses_list = []


        if setting_bet_higher_expected_return_only:
            for i in psorted:
                win_odds = dividend.get_win_odds_by_number(i[0])
                expected_return = 1/float(i[1])
                if win_odds > expected_return:
                    predicted_horses_list.append(i[0])
        else:
            for i in psorted:
                predicted_horses_list.append(i[0])

        if len(predicted_horses_list) < setting_bet_legs:
            continue

        predicted_horses = predicted_horses_list[:setting_bet_legs]

        # there is a theory that the highest/lowest draw has a better chance to win
        dsorted = sorted(draw_list.items(), key=lambda x: x[1], reverse=True)
        highest_draw_last_two = []
        lowest_draw_first_two = []
        for i in dsorted:
            highest_draw_last_two.append(i[0])
            lowest_draw_first_two.append(i[0])
        highest_draw_last_two = highest_draw_last_two[1:3]
        lowest_draw_first_two = lowest_draw_first_two[-2:]

        for x in highest_draw_last_two:
            if x in results_first_three:
                highest_draw_last_two_won = highest_draw_last_two_won + 1
                break

        for x in lowest_draw_first_two:
            if x in results_first_three:
                lowest_draw_first_two_won = lowest_draw_first_two_won + 1
                break

        psorted = sorted(predicted_list.items(), key=lambda x: x[1], reverse=False)
        predicted_last_two = []
        for i in psorted:
            predicted_last_two.append(i[0])
        predicted_last_two = predicted_last_two[:2]

        shuffle(random_first_three)
        #predicted_horses = random_first_three[:3]

        # lost_last_two, predicted accurate that the last two are not in first_three
        r = [x for x in predicted_last_two if x not in results_first_three]
        if len(r) == 2:
            lost_last_two = lost_last_two + 1

        for x in predicted_horses:
            if x in results_first_three:
                bet_legs_won = bet_legs_won + 1
                break

        # dutching & kelly
        predicted_horses_win_odds_list = {}
        predicted_horses_win_odds_average = 0
        for i in predicted_horses:
            predicted_horses_win_odds_list[i] = float(dividend.get_win_odds_by_number(i))

        ori_predicted_horses_win_odds_list = predicted_horses_win_odds_list.copy()

        # simulate delayed hot odds
        if setting_delay_hotodds:
            predicted_horses_win_odds_list_sorted = sorted(predicted_horses_win_odds_list.items(), key=lambda x: x[1], reverse=False)
            for i in predicted_horses_win_odds_list_sorted:
                predicted_horses_win_odds_list[i[0]] = predicted_horses_win_odds_list[i[0]] * setting_delay_hotodds_factor
                break #only adjust the first horse(lowest odds)

        #hotpush
        if setting_hotpush:
            for i in predicted_horses_win_odds_list:
                if floor(predicted_horses_win_odds_list[i]) <= setting_bet_legs:
                    predicted_horses_win_odds_list[i] = predicted_horses_win_odds_list[i] * (setting_hotpush_factor + round(setting_bet_legs/predicted_horses_win_odds_list[i]))

        for i in predicted_horses:
            predicted_horses_win_odds_average = predicted_horses_win_odds_average + predicted_horses_win_odds_list[i]
        predicted_horses_win_odds_average = predicted_horses_win_odds_average/len(predicted_horses)

        kelly_bet = 0
        split_bet = {}
        if setting_bet_kelly and setting_bet_dutching:
            if (bet_principal_accumulated * setting_bet_kelly_fraction) > setting_max_bet_per_race:
                kelly_bet = get_kelly_amount(setting_max_bet_per_race, predicted_horses_win_odds_average, setting_bet_kelly_win_probability)
            else:
                kelly_bet = get_kelly_amount((bet_principal_accumulated * setting_bet_kelly_fraction), predicted_horses_win_odds_average, setting_bet_kelly_win_probability)
            if setting_hotpush or setting_delay_hotodds:
                #restore the hot odds, more accurate for dutching and couter will use split_bet to calculate correct prize
                for i in predicted_horses_win_odds_list:
                    predicted_horses_win_odds_list[i] = ori_predicted_horses_win_odds_list[i]
            split_bet = get_dutching_amount(kelly_bet, predicted_horses_win_odds_list)
        elif setting_bet_kelly and (setting_bet_dutching == False):
            if (bet_principal_accumulated * setting_bet_kelly_fraction) > setting_max_bet_per_race:
                kelly_bet = get_kelly_amount(setting_max_bet_per_race, predicted_horses_win_odds_average, setting_bet_kelly_win_probability)
            else:
                kelly_bet = get_kelly_amount((bet_principal_accumulated * setting_bet_kelly_fraction), predicted_horses_win_odds_average, setting_bet_kelly_win_probability)
            #restore the hot odds, better for dutching and couter will use split_bet to calculate correct prize
            if setting_hotpush or setting_delay_hotodds:
                for i in predicted_horses_win_odds_list:
                    predicted_horses_win_odds_list[i] = ori_predicted_horses_win_odds_list[i]
            split_bet = get_dutching_amount(kelly_bet, predicted_horses_win_odds_list)

            # by default, we revert to bet evenly(no Dutching)
            bet_dutching_mixed_bet_evenly = True
            if setting_bet_dutching_mixed:
                for i in split_bet:
                    if split_bet[i]['odds'] <= setting_bet_legs:
                        bet_dutching_mixed_bet_evenly = False
                        break

            if bet_dutching_mixed_bet_evenly:
                # we have the split_bet array structure, now reset the bet amount without dutching, which means the bet are spread evenly
                for i in split_bet:
                    split_bet[i]['bet'] = round((kelly_bet/setting_bet_legs), -1)

        elif setting_bet_dutching and (setting_bet_kelly == False):
            if setting_hotpush or setting_delay_hotodds:
                #restore the hot odds, better for dutching and couter will use split_bet to calculate correct prize
                for i in predicted_horses_win_odds_list:
                    predicted_horses_win_odds_list[i] = ori_predicted_horses_win_odds_list[i]
            if setting_bet_dutching_ignore_balance:
                split_bet = get_dutching_amount(round(setting_bet_principal, -1), predicted_horses_win_odds_list)
            else:
                split_bet = get_dutching_amount(round(bet_principal_accumulated, -1), predicted_horses_win_odds_list)

        if setting_bet_kelly and kelly_bet <= 0:
            #not worth it, give up this race
            continue

        #if setting_bet_kelly and (bet_principal_accumulated < setting_bet_principal):
        #    print('busted')
        #    break

        # 獨贏, winner
        bet_list = list(itertools.combinations(
            predicted_horses, 1))  # 3 腳 = 3 注
        bet_result = dividend.match_dividend(bet_list, '獨贏')
        if setting_bet_dutching or setting_bet_kelly:
            bet_count_increase('win', meeting_date, bet_list, bet_result, horses_distance, split_bet)
        else:
            bet_count_increase('win', meeting_date, bet_list, bet_result, horses_distance)

        # bet details, only support 'win' at the moment for diagnosing
        if setting_write_bet_details and len(split_bet) > 0:
            for i in split_bet:
                f_won = 0
                if bet_result is not None:
                    for j in bet_result:
                        if int(i) == int(j):
                            f_won = 1
                f_bet_details.write("%s,%s,%s,%s,%s,%s\r\n" % (meeting_date, race_number,i,split_bet[i]['odds'],split_bet[i]['bet'],f_won))

        # 位置, one of first three
        bet_list = list(itertools.combinations(
            predicted_horses, 1))  # 3 腳 = 3 注
        bet_result = dividend.match_dividend(bet_list, '位置')
        bet_count_increase('place', meeting_date, bet_list,
                        bet_result, horses_distance)

        # 連贏, first two, any order
        bet_list = list(itertools.combinations(
            predicted_horses, 2))  # 3 腳 = 3 注
        bet_result = dividend.match_dividend(bet_list, '連贏')
        bet_count_increase('quinella', meeting_date, bet_list,
                        bet_result, horses_distance)

        # 位置Q, any two of first three, any order
        bet_list = list(itertools.combinations(
            predicted_horses, 2))  # 3 腳 = 3 注
        bet_result = dividend.match_dividend(bet_list, '位置Q')
        bet_count_increase('quinella_place', meeting_date,
                        bet_list, bet_result, horses_distance)

        # count of distance/location based on 位置Q success rate with double counting bug, may move this out in future
        if bet_result is not None:
            for i in bet_result:
                distance_count[horses_distance] = distance_count[horses_distance] + 1
                location_count[horses_location] = location_count[horses_location] + 1

        # 單T, first three unordered
        bet_list = list(itertools.combinations(predicted_horses, 3))
        bet_list_today[race_number]['trio'] = bet_list
        bet_result = dividend.match_dividend(bet_list, '單T')

        if setting_use_predicted_variance:
            if np.var(predicted_horses_list) >= setting_use_predicted_variance_min and np.var(predicted_horses_list) <= setting_use_predicted_variance_max:
                bet_count_increase('trio', meeting_date,
                                bet_list, bet_result, horses_distance)
        else:
            bet_count_increase('trio', meeting_date, bet_list,
                            bet_result, horses_distance)

        if bet_result is not None:
            trio_success_variance_for_rating_original.append(
                np.var(rating_original_list))
            trio_success_variance_for_predicted.append(
                np.var(predicted_horses_list))
        else:
            trio_failed_variance_for_rating_original.append(
                np.var(rating_original_list))
            trio_failed_variance_for_predicted.append(
                np.var(predicted_horses_list))

        if setting_full_mode:
            # get the minimum legs
            if setting_bet_legs < 3:
                predicted_horses = predicted_horses_list[:3]

            # 孖T, first three unordered in two selected races
            bet_result = dividend.match_dividend_for_double_trio(
                bet_list_today, race_number)
            dummy_bet_list = list(itertools.product(bet_list, bet_list))
            bet_count_increase('double_trio', meeting_date,
                            dummy_bet_list, bet_result, horses_distance)

            # 三T, first three unordered in three selected races
            bet_result = dividend.match_dividend_for_triple_trio(
                bet_list_today, race_number)
            dummy_bet_list = list(itertools.product(bet_list, bet_list))
            bet_count_increase('triple_trio', meeting_date,
                            dummy_bet_list, bet_result, horses_distance)

            # 三重彩, first three ordered
            bet_list = list(itertools.permutations(predicted_horses, 3))
            bet_result = dividend.match_dividend(bet_list, '三重彩')
            bet_count_increase('tierce', meeting_date, bet_list,
                            bet_result, horses_distance)

            # get the minimum legs
            if setting_bet_legs < 4:
                predicted_horses = predicted_horses_list[:4]

            # 四連環, first four unordered
            bet_list = list(itertools.combinations(predicted_horses, 4))
            bet_result = dividend.match_dividend(bet_list, '四連環')
            bet_count_increase('first_4', meeting_date,
                            bet_list, bet_result, horses_distance)

            # 四重彩, first four ordered
            bet_list = list(itertools.permutations(predicted_horses, 4))
            bet_result = dividend.match_dividend(bet_list, '四重彩')
            bet_count_increase('quartet', meeting_date,
                            bet_list, bet_result, horses_distance)

        number_of_races = number_of_races + 1
        # if number_of_races == 5:
        #    break

    print('場數: %s, 腳: %s, 中1腳: %s' %
        (number_of_races, setting_bet_legs, bet_legs_won))

    total = bet_count_total('win')
    print('獨贏 Rate: %.2f, 注數: %s, 贏: %s, $: %s' %
        ((total['won'] / total['bet']), total['bet'], total['won'], total['prize']))

    total = bet_count_total('place')
    print('位置 Rate: %.2f, 注數: %s, 贏: %s, $: %s' %
        ((total['won'] / total['bet']), total['bet'], total['won'], total['prize']))

    total = bet_count_total('quinella')
    print('連贏 Rate: %.2f, 注數: %s, 贏: %s, $: %s' %
        ((total['won'] / total['bet']), total['bet'], total['won'], total['prize']))

    total = bet_count_total('quinella_place')
    print('位置Q Rate: %.2f, 注數: %s, 贏: %s, $: %s' %
        ((total['won'] / total['bet']), total['bet'], total['won'], total['prize']))

    if setting_bet_legs >= 3:
        total = bet_count_total('trio')
        print('單T Rate: %.2f, 注數: %s, 贏: %s, $: %s' % ((total['won'] / total['bet']), total['bet'], total['won'], total['prize']))

    if setting_full_mode:
        total = bet_count_total('double_trio')
        print('孖T Rate: %.2f, 注數: %s, 贏: %s, $: %s' %
            ((total['won'] / total['bet']), total['bet'], total['won'], total['prize']))

        total = bet_count_total('triple_trio')
        print('三T Rate: %.2f, 注數: %s, 贏: %s, $: %s' %
            ((total['won'] / total['bet']), total['bet'], total['won'], total['prize']))

        total = bet_count_total('tierce')
        print('三重彩 Rate: %.2f, 注數: %s, 贏: %s, $: %s' %
            ((total['won'] / total['bet']), total['bet'], total['won'], total['prize']))

        total = bet_count_total('first_4')
        print('四連環 Rate: %.2f, 注數: %s, 贏: %s, $: %s' %
            ((total['won'] / total['bet']), total['bet'], total['won'], total['prize']))

        total = bet_count_total('quartet')
        print('四重彩 Rate: %.2f, 注數: %s, 贏: %s, $: %s' %
            ((total['won'] / total['bet']), total['bet'], total['won'], total['prize']))

    print('lost_last_two: %.2f' % (lost_last_two/number_of_races))
    print('highest_draw_last_two_won: %.2f' %
        (highest_draw_last_two_won/number_of_races))
    print('lowest_draw_first_two_won: %.2f' %
        (lowest_draw_first_two_won/number_of_races))
    print('distance_count:')
    pprint(distance_count)
    print('location_count:')
    pprint(location_count)

    # print('單T Month count: %s' % bet_count['trio'])
    # print_trio_stats()
    write_day_stats()