# -*- coding: utf-8 -*-
from __future__ import division

from db import Features4 as db_features
from db import Entriesfeatures4 as db_entriesfeatures

from FeaturesCommon import FeaturesCommon

from prepare import parse_distance, label_course, label_location, label_new_horse_race, parse_running_time_to_centisecond
from prepare import get_horse_code, get_jockey_code, get_trainer_code
from prepare import find_horse_jockey_race_result, find_horse_race_result, find_horse_race_same_distance_result, get_previous_race
from prepare import find_days_since_last_race, find_horse_class_adjustment

# features4 inherit from features3 with previous racing speed
# TODO: metre_per_centisecond_previously is not relative
# TODO: verify accuracy of metre_per_centisecond_previously_same_distance


class Features4(FeaturesCommon):
    def __init__(self):
        self.db_features = db_features
        self.db_entriesfeatures = db_entriesfeatures
        self.training_epochs = 40
        self.features_name = 'features4'
        self.drop_columns = ['id', 'race_id', 'result_id', 'meeting_date', 'result_first_three', 'result_won', 'new_horse_race',
                             'horse_code', 'jockey_code', 'trainer_code']

    def prepare_features(self, row, meeting_date, distance, course, location, number_of_horses, horses_class, entry_rating):
        distance = parse_distance(distance)
        course = label_course(course)
        location = label_location(location)
        draw = row.draw
        draw_relative = draw/number_of_horses
        number = row.number
        number_relative = number/number_of_horses
        actual_weight = row.actual_weight
        declared_weight = row.declared_weight
        actual_over_declared_weight = actual_weight/declared_weight
        new_horse_race = label_new_horse_race(horses_class)
        horse_jockey_race_played = 0
        horse_jockey_race_won = 0
        horse_jockey_race_first_three = 0
        horse_jockey_race_won_ratio = 0
        horse_jockey_race_first_three_ratio = 0
        horse_race_played = 0
        horse_race_won = 0
        horse_race_first_three = 0
        horse_race_won_ratio = 0
        horse_race_first_three_ratio = 0
        horse_race_same_distance_played = 0
        horse_race_same_distance_won = 0
        horse_race_same_distance_first_three = 0
        horse_race_same_distance_won_ratio = 0
        horse_race_same_distance_first_three_ratio = 0
        horse_code = get_horse_code(row.horse_id)
        jockey_code = get_jockey_code(row.jockey_id)
        trainer_code = get_trainer_code(row.trainer_id)
        rating = entry_rating[0][str(number)]
        rating_original = entry_rating[1][str(number)]
        rating_original_variance = entry_rating[5]
        rating_adjusted_rate = entry_rating[2][str(number)]
        rating_relative = entry_rating[3][str(number)]
        rating_original_relative = entry_rating[4][str(number)]
        days_since_last_race = 0
        horse_class_adjustment = 0
        metre_per_centisecond_previously = 0
        metre_per_centisecond_previously_same_distance = 0
        horse_race_same_distance_won_previously = 0
        horse_race_same_distance_first_three_previously = 0
        horse_jockey_race_won_previously = 0
        horse_jockey_race_first_three_previously = 0
        horse_race_won_previously = 0
        horse_race_first_three_previously = 0

        if new_horse_race == 0:
            horse_jockey_race_played, horse_jockey_race_won, horse_jockey_race_first_three = find_horse_jockey_race_result(
                meeting_date, row.horse_id, row.jockey_id)
            horse_race_played, horse_race_won, horse_race_first_three = find_horse_race_result(
                meeting_date, row.horse_id)
            horse_race_same_distance_played, horse_race_same_distance_won, horse_race_same_distance_first_three = find_horse_race_same_distance_result(
                meeting_date, row.horse_id, distance)
            days_since_last_race = find_days_since_last_race(
                row.horse_id, meeting_date)
            horse_class_adjustment = find_horse_class_adjustment(
                row.horse_id, meeting_date, horses_class)
            previous_race = get_previous_race(meeting_date, row.horse_id)
            if previous_race is not None:
                previous_race_distance = parse_distance(
                    previous_race['race'].distance)
                previous_race_distance = int(previous_race_distance)
                previous_race_finish_time = previous_race['result'].finish_time
                previous_race_finish_time = parse_running_time_to_centisecond(
                    previous_race_finish_time)
                if previous_race_distance == int(distance):
                    if previous_race_finish_time is not None:
                        metre_per_centisecond_previously = previous_race_distance / previous_race_finish_time
                        metre_per_centisecond_previously_same_distance = 1
                    if previous_race['result'].place == 1:
                        horse_race_same_distance_won_previously = 1
                    if previous_race['result'].place <= 3:
                        horse_race_same_distance_first_three_previously = 1
                if previous_race['result'].jockey_id == row.jockey_id:
                    if previous_race['result'].place == 1:
                        horse_jockey_race_won_previously = 1
                    if previous_race['result'].place <= 3:
                        horse_jockey_race_first_three_previously = 1
                if previous_race['result'].place == 1:
                    horse_race_won_previously = 1
                if previous_race['result'].place <= 3:
                    horse_race_first_three_previously = 1

        if horse_jockey_race_played > 0:
            horse_jockey_race_won_ratio = horse_jockey_race_won/horse_jockey_race_played
            horse_jockey_race_first_three_ratio = horse_jockey_race_first_three / \
                horse_jockey_race_played

        if horse_race_played > 0:
            horse_race_won_ratio = horse_race_won/horse_race_played
            horse_race_first_three_ratio = horse_race_first_three/horse_race_played

        if horse_race_same_distance_played > 0:
            horse_race_same_distance_won_ratio = horse_race_same_distance_won / \
                horse_race_same_distance_played
            horse_race_same_distance_first_three_ratio = horse_race_same_distance_first_three / \
                horse_race_same_distance_played

        # figures from days_since_last_race_studies.xls
        if days_since_last_race == 0 or days_since_last_race >= 120:
            days_since_last_race = 4
        elif days_since_last_race >= 45 < 120:
            days_since_last_race = 3
        elif days_since_last_race >= 30 < 45:
            days_since_last_race = 2
        else:
            days_since_last_race = 1

        data = {
            'meeting_date': meeting_date, 'meeting_month': meeting_date.month, 'number_of_horses': number_of_horses,
            'distance': distance, 'course': course, 'location': location,
            'draw': draw, 'number': number, 'number_relative': number_relative, 'draw_relative': draw_relative,
            'actual_weight': actual_weight, 'declared_weight': declared_weight, 'actual_over_declared_weight': actual_over_declared_weight,
            'new_horse_race': new_horse_race, 'horse_code': horse_code, 'jockey_code': jockey_code, 'trainer_code': trainer_code,
            'horse_jockey_race_played': horse_jockey_race_played, 'horse_jockey_race_won': horse_jockey_race_won, 'horse_jockey_race_first_three': horse_jockey_race_first_three,
            'horse_jockey_race_won_ratio': horse_jockey_race_won_ratio, 'horse_jockey_race_first_three_ratio': horse_jockey_race_first_three_ratio,
            'horse_race_played': horse_race_played, 'horse_race_won': horse_race_won, 'horse_race_first_three': horse_race_first_three,
            'horse_race_won_ratio': horse_race_won_ratio, 'horse_race_first_three_ratio': horse_race_first_three_ratio,
            'horse_race_same_distance_played': horse_race_same_distance_played, 'horse_race_same_distance_won': horse_race_same_distance_won, 'horse_race_same_distance_first_three': horse_race_same_distance_first_three,
            'horse_race_same_distance_won_ratio': horse_race_same_distance_won_ratio, 'horse_race_same_distance_first_three_ratio': horse_race_same_distance_first_three_ratio,
            'rating': rating, 'rating_original': rating_original, 'rating_original_variance': rating_original_variance, 'rating_adjusted_rate': rating_adjusted_rate, 'rating_relative': rating_relative, 'rating_original_relative': rating_original_relative,
            'days_since_last_race': days_since_last_race, 'horse_class_adjustment': horse_class_adjustment,
            'metre_per_centisecond_previously': metre_per_centisecond_previously, 'metre_per_centisecond_previously_same_distance': metre_per_centisecond_previously_same_distance,
            'horse_jockey_race_won_previously': horse_jockey_race_won_previously, 'horse_jockey_race_first_three_previously': horse_jockey_race_first_three_previously,
            'horse_race_won_previously': horse_race_won_previously, 'horse_race_first_three_previously': horse_race_first_three_previously,
            'horse_race_same_distance_won_previously': horse_race_same_distance_won_previously, 'horse_race_same_distance_first_three_previously': horse_race_same_distance_first_three_previously,
        }

        return data


f = Features4()
f.main()
