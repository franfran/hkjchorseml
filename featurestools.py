"""
    auto generate additional features using featuretools
"""
import pandas as pd
import numpy as np
import featuretools as ft
from featuretools import Feature
import warnings

warnings.filterwarnings('ignore')

df = pd.read_csv('features1_train.csv')
auto_train_data = df.drop(
    columns=['race_id', 'result_id', 'meeting_date', 'result_first_three', 'result_won'])
auto_train_data_ignored = df[['race_id', 'result_id',
                              'meeting_date', 'result_first_three', 'result_won']]

df = pd.read_csv('features1_test.csv')
auto_test_data = df.drop(
    columns=['race_id', 'result_id', 'meeting_date', 'result_first_three', 'result_won'])
auto_test_data_ignored = df[['race_id', 'result_id',
                             'meeting_date', 'result_first_three', 'result_won']]

# print(ft.list_primitives())

# initialize an EntitySet and give it an id
es = ft.EntitySet(id='train_data')
# load dataframe as an entity
es = es.entity_from_dataframe(
    entity_id='train_data', dataframe=auto_train_data, index='id')
auto_train_data, features = ft.dfs(
    entityset=es, target_entity='train_data', verbose=True,
    max_depth=3,
    agg_primitives=["max", "mode", "std", "mean", "skew",
                    "trend", "sum", "median", "count"],
    trans_primitives=['cum_count']
)

es2 = ft.EntitySet(id='test_data')
es2 = es2.entity_from_dataframe(
    entity_id='test_data', dataframe=auto_test_data, index='id')
auto_test_data, features = ft.dfs(
    entityset=es2, target_entity='test_data', verbose=True,
    max_depth=3,
    agg_primitives=["max", "mode", "std", "mean", "skew",
                    "trend", "sum", "median", "count"],
    trans_primitives=['cum_count']
)

# 数据对齐
# auto_train_data, auto_test_data = auto_train_data.align(
#    auto_test_data, join='inner', axis=1)

# auto_train_data.shape
# auto_test_data.shape
print(auto_train_data.head())

auto_test_data = pd.concat(
    [auto_test_data, auto_test_data_ignored], axis=1, sort=False, join='inner')
auto_train_data = pd.concat(
    [auto_train_data, auto_train_data_ignored], axis=1, sort=False, join='inner')

auto_test_data.to_csv('features1_auto_test.csv', index=False)
auto_train_data.to_csv('features1_auto_train.csv', index=False)
