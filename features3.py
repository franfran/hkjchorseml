# -*- coding: utf-8 -*-
from __future__ import division

from db import Features3 as db_features
from db import Entriesfeatures3 as db_entriesfeatures

from FeaturesCommon import FeaturesCommon

from prepare import parse_distance, label_course, label_location, label_new_horse_race
from prepare import get_horse_code, get_jockey_code, get_trainer_code
from prepare import find_horse_jockey_race_result, find_horse_race_result, find_horse_race_same_distance_result
from prepare import find_days_since_last_race, find_horse_class_adjustment

# features3 inherit from features2 with 'days_since_last_race' and 'horse_class_adjustment' values added


class Features3(FeaturesCommon):
    def __init__(self):
        self.db_features = db_features
        self.db_entriesfeatures = db_entriesfeatures
        self.features_name = 'features3'
        self.drop_columns = ['id', 'race_id', 'result_id', 'meeting_date', 'result_first_three', 'result_won', 'new_horse_race',
                             'horse_code', 'jockey_code', 'trainer_code', 'number_of_horses']

    def prepare_features(self, row, meeting_date, distance, course, location, number_of_horses, horses_class, entry_rating):
        distance = parse_distance(distance)
        course = label_course(course)
        location = label_location(location)
        draw = row.draw
        draw_relative = draw/number_of_horses
        number = row.number
        number_relative = number/number_of_horses
        actual_weight = row.actual_weight
        declared_weight = row.declared_weight
        actual_over_declared_weight = actual_weight/declared_weight
        new_horse_race = label_new_horse_race(horses_class)
        horse_jockey_race_played = 0
        horse_jockey_race_won = 0
        horse_jockey_race_first_three = 0
        horse_jockey_race_won_ratio = 0
        horse_jockey_race_first_three_ratio = 0
        horse_race_played = 0
        horse_race_won = 0
        horse_race_first_three = 0
        horse_race_won_ratio = 0
        horse_race_first_three_ratio = 0
        horse_race_same_distance_played = 0
        horse_race_same_distance_won = 0
        horse_race_same_distance_first_three = 0
        horse_race_same_distance_won_ratio = 0
        horse_race_same_distance_first_three_ratio = 0
        horse_code = get_horse_code(row.horse_id)
        jockey_code = get_jockey_code(row.jockey_id)
        trainer_code = get_trainer_code(row.trainer_id)
        rating = entry_rating[0][str(number)]
        rating_original = entry_rating[1][str(number)]
        rating_original_variance = entry_rating[5]
        rating_adjusted_rate = entry_rating[2][str(number)]
        rating_relative = entry_rating[3][str(number)]
        rating_original_relative = entry_rating[4][str(number)]
        days_since_last_race = 0
        horse_class_adjustment = 0

        if new_horse_race == 0:
            horse_jockey_race_played, horse_jockey_race_won, horse_jockey_race_first_three = find_horse_jockey_race_result(
                meeting_date, row.horse_id, row.jockey_id)
            horse_race_played, horse_race_won, horse_race_first_three = find_horse_race_result(
                meeting_date, row.horse_id)
            horse_race_same_distance_played, horse_race_same_distance_won, horse_race_same_distance_first_three = find_horse_race_same_distance_result(
                meeting_date, row.horse_id, distance)
            days_since_last_race = find_days_since_last_race(
                row.horse_id, meeting_date)
            horse_class_adjustment = find_horse_class_adjustment(
                row.horse_id, meeting_date, horses_class)

        if horse_jockey_race_played > 0:
            horse_jockey_race_won_ratio = horse_jockey_race_won/horse_jockey_race_played
            horse_jockey_race_first_three_ratio = horse_jockey_race_first_three / \
                horse_jockey_race_played

        if horse_race_played > 0:
            horse_race_won_ratio = horse_race_won/horse_race_played
            horse_race_first_three_ratio = horse_race_first_three/horse_race_played

        if horse_race_same_distance_played > 0:
            horse_race_same_distance_won_ratio = horse_race_same_distance_won / \
                horse_race_same_distance_played
            horse_race_same_distance_first_three_ratio = horse_race_same_distance_first_three / \
                horse_race_same_distance_played

        # figures from days_since_last_race_studies.xls
        if days_since_last_race == 0 or days_since_last_race >= 120:
            days_since_last_race = 4
        elif days_since_last_race >= 45 < 120:
            days_since_last_race = 3
        elif days_since_last_race >= 30 < 45:
            days_since_last_race = 2
        else:
            days_since_last_race = 1

        data = {
            'meeting_date': meeting_date, 'meeting_month': meeting_date.month, 'number_of_horses': number_of_horses,
            'distance': distance, 'course': course, 'location': location,
            'draw': draw, 'number': number, 'number_relative': number_relative, 'draw_relative': draw_relative,
            'actual_weight': actual_weight, 'declared_weight': declared_weight, 'actual_over_declared_weight': actual_over_declared_weight,
            'new_horse_race': new_horse_race, 'horse_code': horse_code, 'jockey_code': jockey_code, 'trainer_code': trainer_code,
            'horse_jockey_race_played': horse_jockey_race_played, 'horse_jockey_race_won': horse_jockey_race_won, 'horse_jockey_race_first_three': horse_jockey_race_first_three,
            'horse_jockey_race_won_ratio': horse_jockey_race_won_ratio, 'horse_jockey_race_first_three_ratio': horse_jockey_race_first_three_ratio,
            'horse_race_played': horse_race_played, 'horse_race_won': horse_race_won, 'horse_race_first_three': horse_race_first_three,
            'horse_race_won_ratio': horse_race_won_ratio, 'horse_race_first_three_ratio': horse_race_first_three_ratio,
            'horse_race_same_distance_played': horse_race_same_distance_played, 'horse_race_same_distance_won': horse_race_same_distance_won, 'horse_race_same_distance_first_three': horse_race_same_distance_first_three,
            'horse_race_same_distance_won_ratio': horse_race_same_distance_won_ratio, 'horse_race_same_distance_first_three_ratio': horse_race_same_distance_first_three_ratio,
            'rating': rating, 'rating_original': rating_original, 'rating_original_variance': rating_original_variance, 'rating_adjusted_rate': rating_adjusted_rate, 'rating_relative': rating_relative, 'rating_original_relative': rating_original_relative,
            'days_since_last_race': days_since_last_race, 'horse_class_adjustment': horse_class_adjustment,
        }

        return data


f = Features3()
f.main()
